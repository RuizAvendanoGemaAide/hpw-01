/*Tipos de datos en JavaScript*/
typeof "Hola mundo";

typeof [10, 20];

typeof 10.3;

typeof {};

typeof null;

typeof undefined;

typeof NaN;

x = undefined;

typeof x;

3*2;

3*2+undefined;

x = 5;

y = 4;

x + y;

x + y + NaN;

!true + !false;

!0;

!1;

!"Hola mundo";

!"";

"hola mundo" == "";

!3;

!-3;

![20, 30];

!false + !false;

!null;

!undefined;

año = 2014;

!r;

apellido_paterno = "xxxxxxx";

apellido-paterno= 10;

apellido_paterno;

15/2;

15%2;

true < false;

false < true;

x = 23;

x++;

"A" < "B";

3==3;

3 == "3"; //compara si son iguales en caracteres

3==="3"; //compara si son iguales en caracteres pero tambien en tipos de datos.

0 == false;

0 === false;

"" == false;

"" === false;

NaN == NaN;

NaN === NaN;

undefined == undefined;

undefined === undefined;

x = undefined || 5;

[10, 20, 30].length; // cuantos elementos tiene un arreglo


[10, 20, 30][0]; //acceso a elementos de un arreglo

[10, , 30][1];

![10, , 30][1];


xs= [10, 20, 30];

ys= [[10, 20],[30, 40, 50],60,[70]];

ys [1][1];

ys[4];

ys[4]= [100, 200, 300];

ys;

ys[7]= [-1, -2, -3];

objA = {"nombre": "gema", "apellidos": "ruiz avendaño"};

objA ["apellidos"];//entrar a un elemento de un arreglo.

objB = [{"nombre" : "Gema Aide Ruiz Avendaño"},{"No_Control"
                                               :"09161278"}
       ];

arr = [adf,asd];



alumno= {"NoControl":"09161133","Nombre":{"nombre":"gema","apelldos":{"Paterno":"ruiz","Materno":"avendaño"}},
         "materias":[{"clave":"abc123","nombre":"Matematicas","calif":"80"},{"clave":"abc345","nombre":"Quimica","calif":"90"}
                     ,{"clave":"sdf890","nombre":"PW","calif":"80"}]};


//Otra forma de hacer arreglos

Array(10, 20, 30);



-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*Arreglo Multidimensional*/
xs = [10, 20, 30, 40, 50];

ys = [
    [11,22,33]
    [44,55,66]
    [77,88,99]
    ];

/*Recorrdio con for*/
for (i=0; i<ys.length; i++){
    for(j=0; j<ys[i].length; j++){
    console.log(ys[i][j]);
}
}

/*Recorrido con for-earch*/
/*... menos recomendado*/

for each (arreglo in ys){
    for each (elemento in arreglo){
        console.log(elemento);
    }
}

window.ys;

/*var*/
for each (var arreglo in ys){
    for each (var elemento in arreglo){
        console.log(elemento);
    }
}

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*Recorrido de Arreglos*/

obj= {
    "clave":"12344",
    "nombre":"gema aide",
    "Estado":"mmmmm"
}

for (var llave in obj){
    console.log("llave: "+llave);
    valor= obj[llave];
    console.log("valor: " + valor);
}

xs = [10, 20, 30];
for (var indice in xs){
    console.log(xs[indice]);
}

if(!obj["grupos"]){
    console.log(":)...");
}else{
    console.log("No tiene Grupos el profesor");
}




profesor= {"Clave":"HBA1234","NombreProfe":{"nombre":"Antonio","apellidos":{"Paterno":"Hernandez","Materno":"Blas"}}
"Departamento":"Sistemas Computacionales","Grupos":"ISA, ISB, ISC"
};

grupo= {"Clave":"ABC678","Aula":"CC","Horario":"10:00-13:30","Alumnos":["Banchez Cruz Pablo","Ruiz Avendaño Gema",
"lopez solis luis"]};

alumno= {"NoControl":["09161278 ruiz avendaño gema aide","09161278 Gema Ruiz Avendaño"]};

departamento={departamentos:{"nombre":"Sistemas Computacionales","clave":"ABG0823"} };

/* recorrido de un for RECORRIDO DE ARREGLOS*/
for (var i = 0; i < ys.length; i++){
    for (var)
}

obj ={
    "clave": "09161278",
    "nombre": "ruiz avendaño gema aide",
    "estado": "oaxaca"
}
for ( var llave in obj){
    console.log("llave" + llave);
    valor =obj[llave];
    console.log("valor:" + valor);
}
xs = [10 , 20 , 30];
for(var indice in xs){
    console.log (xs [indice]);
}

if (!obj["grupos"]){
    console.log (":)....");}
    else {
        console.log ("no tiene grupos el profesor");
    }
    
    
/*DEFINICION DE UN FUNCION*/

function nombre_de_funcion(parametros){
    cuerpo de la funcion*/
    
}

........................................
function nombre_de_funcion(parametros){
    console.log(parametros);
    return parametros;
}

nombre_de_funcion(10);
.........................................
function nombre_de_funcion(parametro1,parametro2){
   
    return parametro1+parametro2;
}

nombre_de_funcion(20, 30);

........................................
function nombre_de_funcion(parametro1,parametro2){
   
    return parametro1+parametro2;
}

nombre_de_funcion(true, true);

.........................................
function nombre_de_funcion(parametro1,parametro2){
   
    return parametro1+ parametro2;
}

nombre_de_funcion("hola", " mundo");
...............................................
function numero_grupos(profesor){
    if (profesor["grupos"]){
        return profesor ["grupos"].length;
    }
    return 0;
}
obj_profesor1 ={
    "clave": "12345",
    "nombre": "antonio hernandez blas",
    "grupos": [
        {"clave":"1", "nombre": "a"},
        {"clave":"2", "nombre": "b"}
        ]
}
obj_profesor2 ={
    "clave": "12345",
    "nombre": "antonio hernandez blas",
    "grupos": [        ]
}

obj_profesor3 ={
    "clave": "12345",
    "nombre": "antonio hernandez blas"
    }
    
console.log(numero_grupos(obj_profesor1));
console.log(numero_grupos(obj_profesor2));
console.log(numero_grupos(obj_profesor3));


............................................

function numero_alumnos(profesor){
    if (profesor["grupos"]){
        if("grupos"["clave"]){
            if(["clave"["alumnos"]])
            {
                        return profesor ["alumnos"].length;

            }
        }
        
    }
    return 20;
}

...............................................
function numero_alumnos(profesor){
    if (profesor["grupos"["clave"["alumnos"]]]){
        
                        return profesor ["alumnos"].length;

            }
             return 20;
        }
        
    
   


obj_profesor1 ={
    "clave": "12345",
    "nombre": "antonio hernandez blas",
    "grupos": [
        {"clave":"1", "nombre": "a","alumnos": [
          {"no_control": "09161278","nombre": "ruiz avendaño gema aide"},
          {"no_control": "09161275","nombre": "ruiz avendaño "},
          {"no_control": "09161133","nombre": "banchez cruz pablo zaid "},
          {"no_control": "09161226","nombre": "martinez hernandez patricia nayeli"}
          ]},
        {"clave":"2", "nombre": "b"}
        ]
        
}
obj_profesor2 ={
    "clave": "12345",
    "nombre": "antonio hernandez blas",
    "grupos": [        ]
}

obj_profesor3 ={
    "clave": "12345",
    "nombre": "antonio hernandez blas"
    }
    
console.log(numero_alumnos(obj_profesor1,"1"));
console.log(numero_alumnos(obj_profesor2));
console.log(numero_numero de alumnos(obj_profesor3));


/*entorno global
var x=10;
/*entorno global*/
function f(x){
    entorno local*/
   var x=5;
    y*=x;
    console.log(y);
}


 
/*entorno global
 f(x);

/*funcion constructoras*/
function profesor(clave, nombre, grupos){
    return{ 
        "clave": clave,
        "nombre": nombre,
        "grupos": grupos
        }
}
obj_profesor1 = profesor("12345", "antonio hernandez blas","isa, isb, isc");
console.log(obj_profesor1 ["nombre"]);
 